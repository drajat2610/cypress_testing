.. Cypress_Testing documentation master file, created by
   sphinx-quickstart on Thu Nov  2 14:52:59 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Cypress_Testing's documentation!
===========================================

.. toctree::
   :maxdepth: 2
   
   introduction
   installation
   usage
   api_reference


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
